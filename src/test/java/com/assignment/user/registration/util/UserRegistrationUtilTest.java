package com.assignment.user.registration.util;

import com.assignment.user.registration.domain.RestResponse;
import com.assignment.user.registration.entities.User;
import com.assignment.user.registration.entities.UserExclusion;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Map;

import static com.assignment.user.registration.domain.ApplicationConstants.ERROR_CODE;
import static com.assignment.user.registration.domain.ApplicationConstants.ERROR_MESSAGE;
import static com.assignment.user.registration.domain.ValidationError.USER_NAME_VALIDATION_ERROR;
import static com.assignment.user.registration.util.UserRegistrationUtil.*;
import static java.util.Arrays.asList;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
public class UserRegistrationUtilTest {

    @Test
    public void testBuildUser() {
        String userName = "userName";
        String password = "password";
        String dob = "1992-12-21";
        String ssn = "ssn";
        LocalDate regDate = LocalDate.now();

        User user = buildUser(userName, password, dob,ssn, regDate, true);
        assertNotNull(user);
        assertEquals(userName, user.getUserName());
        assertEquals(password, user.getPassword());
        assertEquals(LocalDate.of(1992, 12, 21), user.getDateOfBirth());
        assertEquals(ssn, user.getSocialSecurityNumber());
        assertTrue(user.isActive());
    }

    @Test
    public void testBuildExclusion() {
        String dob = "1992-12-21";
        String ssn = "ssn";

        UserExclusion exclusion = buildExclusion(dob,ssn);
        assertNotNull(exclusion);
        assertEquals(LocalDate.of(1992, 12, 21), exclusion.getDateOfBirth());
        assertEquals(ssn, exclusion.getSocialSecurityNumber());
    }

    @Test
    public void testBuildRestResponse() {
        HttpStatus status = HttpStatus.BAD_REQUEST;
        Map<String, String> errorDetails = getErrorDetails(USER_NAME_VALIDATION_ERROR);
        RestResponse restResponse = buildRestResponse(status.value(), status.getReasonPhrase(), asList(errorDetails));
        assertNotNull(restResponse);
        assertEquals(asList(errorDetails), restResponse.getErrorDetails());
        assertEquals(status.value(), restResponse.getStatus());
        assertEquals(status.getReasonPhrase(), restResponse.getStatusDescription());
    }

    @Test
    public void testGetErrorDetails() {
        Map<String, String> errorDetails = getErrorDetails(USER_NAME_VALIDATION_ERROR);
        assertNotNull(errorDetails);
        assertEquals(2, errorDetails.size());
        assertEquals(USER_NAME_VALIDATION_ERROR.getErrorCode(), errorDetails.get(ERROR_CODE));
        assertEquals(USER_NAME_VALIDATION_ERROR.getErrorMessage(), errorDetails.get(ERROR_MESSAGE));
    }

    @Test
    public void testDateParse() {
        parseDate("1992-12-21");
    }

    @Test(expected = DateTimeParseException.class)
    public void testDateParseException() {
        parseDate("1992-21-21");
    }

}
