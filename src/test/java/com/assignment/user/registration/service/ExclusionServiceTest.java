package com.assignment.user.registration.service;

import com.assignment.user.registration.dao.UserExclusionDao;
import com.assignment.user.registration.entities.UserExclusion;
import com.assignment.user.registration.service.validation.ValidationService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class ExclusionServiceTest {

    @TestConfiguration
    static class ExclusionServiceTestBeanConfiguration {
        @MockBean
        UserExclusionDao userExclusionDao;

        @MockBean
        ValidationService validationService;

        @Bean
        public ExclusionServiceImpl ExclusionServiceImpl() {
            return new ExclusionServiceImpl(userExclusionDao, validationService);
        }
    }

    @Autowired
    ExclusionService exclusionService;

    @Autowired
    UserExclusionDao userExclusionDao;

    @Autowired
    ValidationService validationService;

    @Test
    public void testGetAll(){
        when(userExclusionDao.getAll()).thenReturn(Arrays.asList(new UserExclusion(), new UserExclusion()));
        when(validationService.validateDateOfBirth(anyString())).thenReturn(null);
        List<UserExclusion> exclusions = exclusionService.getAll();
        assertNotNull(exclusions);
        assertEquals(2, exclusions.size());
    }

    @Test
    public void testGetExclusion() {
        when(userExclusionDao.findByDateOfBirthAndSsn(any(LocalDate.class), anyString())).thenReturn(new UserExclusion());
        when(validationService.validateDateOfBirth(anyString())).thenReturn(null);
        assertNotNull(exclusionService.getExclusion("1992-12-21", "ssn"));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetExclusionException() {
        when(userExclusionDao.findByDateOfBirthAndSsn(any(LocalDate.class), anyString())).thenThrow(EntityNotFoundException.class);
        when(validationService.validateDateOfBirth(anyString())).thenReturn(null);
        assertNotNull(exclusionService.getExclusion("1992-12-21", "ssn"));
    }

    @Test
    public void testAddExclusion() {
        doNothing().when(userExclusionDao).addExclusion(any(UserExclusion.class));
        when(validationService.validateDateOfBirth(anyString())).thenReturn(null);
        exclusionService.addExclusion("1992-12-21", "ssn");
        verify(userExclusionDao, times(1)).addExclusion(any(UserExclusion.class));
    }

    @Test
    public void testRemoveExclusion() {
        when(userExclusionDao.findByDateOfBirthAndSsn(any(LocalDate.class), anyString())).thenReturn(new UserExclusion());
        when(validationService.validateDateOfBirth(anyString())).thenReturn(null);
        doNothing().when(userExclusionDao).removeExclusion(any(UserExclusion.class));
        exclusionService.removeExclusion("1992-12-21", "ssn");
        verify(userExclusionDao, times(1)).removeExclusion(any(UserExclusion.class));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveExclusionException() {
        when(validationService.validateDateOfBirth(anyString())).thenReturn(null);
        when(userExclusionDao.findByDateOfBirthAndSsn(any(LocalDate.class), anyString())).thenThrow(EntityNotFoundException.class);
        doNothing().when(userExclusionDao).removeExclusion(any(UserExclusion.class));
        exclusionService.removeExclusion("1992-12-21", "ssn");
    }

    @Test
    public void testValidScenario() {
        when(userExclusionDao.findByDateOfBirthAndSsn(any(LocalDate.class), anyString())).thenReturn(new UserExclusion());
        when(validationService.validateDateOfBirth(anyString())).thenReturn(null);
        assertTrue(exclusionService.validate("1992-12-21", "ssn"));
    }

    @Test
    public void testInValidScenario() {
        when(userExclusionDao.findByDateOfBirthAndSsn(any(LocalDate.class), anyString())).thenThrow(EntityNotFoundException.class);
        when(userExclusionDao.getAll()).thenReturn(Collections.emptyList());
        when(validationService.validateDateOfBirth(anyString())).thenReturn(null);
        assertFalse(exclusionService.validate("1992-12-21", "ssn"));
    }
}
