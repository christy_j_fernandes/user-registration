package com.assignment.user.registration.service;

import com.assignment.user.registration.dao.RegistrationDao;
import com.assignment.user.registration.entities.User;
import com.assignment.user.registration.exceptions.BadRequestException;
import com.assignment.user.registration.exceptions.UserPresentInExclusionListException;
import com.assignment.user.registration.service.validation.ValidationService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityNotFoundException;

import static com.assignment.user.registration.domain.ValidationError.USER_NAME_VALIDATION_ERROR;
import static java.util.Arrays.asList;
import static org.junit.Assert.assertFalse;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class RegistrationServiceTest {

    @TestConfiguration
    static class RegistrationServiceTestBeanConfiguration {
        @MockBean
        ExclusionService exclusionService;

        @MockBean
        ValidationService validationService;

        @MockBean
        RegistrationDao registrationDao;

        @Bean
        public RegistrationServiceImpl RegistrationServiceImpl() {
            return new RegistrationServiceImpl(registrationDao, exclusionService, validationService);
        }
    }

    @Autowired
    RegistrationService registrationService;

    @Autowired
    ExclusionService exclusionService;

    @Autowired
    ValidationService validationService;

    @Autowired
    RegistrationDao registrationDao;

    @Test
    public void testRegisterUser(){
        when(validationService.getValidationErrorsForRegisterUser(anyString(), anyString(), anyString())).thenReturn(null);
        when(exclusionService.validate(anyString(), anyString())).thenReturn(false);
        doNothing().when(registrationDao).createOrUpdateUser(any(User.class));

        registrationService.registerUser("username", "password", "1992-12-21", "ssn");
        verify(validationService, times(1)).getValidationErrorsForRegisterUser(anyString(), anyString(), anyString());
        verify(exclusionService, times(1)).validate(anyString(), anyString());
        verify(registrationDao, times(1)).createOrUpdateUser(any(User.class));
    }

    @Test(expected = BadRequestException.class)
    public void testRegisterUserBadRequestException(){
        when(validationService.getValidationErrorsForRegisterUser(anyString(), anyString(), anyString())).thenReturn(asList(USER_NAME_VALIDATION_ERROR));
        when(exclusionService.validate(anyString(), anyString())).thenReturn(false);
        doNothing().when(registrationDao).createOrUpdateUser(any(User.class));

        registrationService.registerUser("username", "password", "1992-12-21", "ssn");
    }

    @Test(expected = UserPresentInExclusionListException.class)
    public void testRegisterUserExclusionListException(){
        when(validationService.getValidationErrorsForRegisterUser(anyString(), anyString(), anyString())).thenReturn(null);
        when(exclusionService.validate(anyString(), anyString())).thenReturn(true);
        doNothing().when(registrationDao).createOrUpdateUser(any(User.class));

        registrationService.registerUser("username", "password", "1992-12-21", "ssn");
    }

    @Test
    public void testDeactivateUser(){
        User user = new User();
        user.setActive(true);
        when(registrationDao.getUser(anyLong())).thenReturn(user);
        registrationService.deactivateUser(1L);
        assertFalse(user.isActive());
        verify(registrationDao, times(1)).getUser(anyLong());
        verify(registrationDao, times(1)).createOrUpdateUser(any(User.class));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeactivateUserException(){
        when(registrationDao.getUser(anyLong())).thenThrow(EntityNotFoundException.class);
        registrationService.deactivateUser(1L);
    }

}
