package com.assignment.user.registration.service.validation;

import com.assignment.user.registration.domain.ValidationError;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static com.assignment.user.registration.domain.ApplicationConstants.DEFAULT_PASSWORD_REGEX;
import static com.assignment.user.registration.domain.ApplicationConstants.DEFAULT_USER_NAME_REGEX;
import static com.assignment.user.registration.domain.ValidationError.*;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
public class ValidationServiceTest {

    @TestConfiguration
    static class UserValidationServiceTestBeanConfiguration {
        @Bean
        public ValidationServiceImpl UserValidationServiceImpl() {
            return new ValidationServiceImpl(DEFAULT_USER_NAME_REGEX, DEFAULT_PASSWORD_REGEX);
        }
    }

    @Autowired
    ValidationService validationService;

    @Test
    public void testGetValidationErrors(){
        List<ValidationError> validationErrors = validationService.getValidationErrorsForRegisterUser("test123", "Abc123", "1992-12-21");
        assertNull(validationErrors);
    }

    // User name with special symbol '_'
    @Test
    public void userNameAlphaNumericSpecialSymbolFailure() {
        List<ValidationError> validationErrors = validationService.getValidationErrorsForRegisterUser("test_123", "Abc123", "1992-12-21");
        assertNotNull(validationErrors);
        assertEquals(1, validationErrors.size());
        assertEquals(USER_NAME_VALIDATION_ERROR.getErrorMessage(), validationErrors.get(0).getErrorMessage());
        assertEquals(USER_NAME_VALIDATION_ERROR.getErrorCode(), validationErrors.get(0).getErrorCode());
    }

    // User name with space
    @Test
    public void userNameAlphaNumericSpaceFailure() {
        List<ValidationError> validationErrors = validationService.getValidationErrorsForRegisterUser("test 123", "Abc123", "1992-12-21");
        assertNotNull(validationErrors);
        assertEquals(1, validationErrors.size());
        assertEquals(USER_NAME_VALIDATION_ERROR.getErrorMessage(), validationErrors.get(0).getErrorMessage());
        assertEquals(USER_NAME_VALIDATION_ERROR.getErrorCode(), validationErrors.get(0).getErrorCode());
    }

    // password with no small case letter
    @Test
    public void passwordOneSmallCaseFailure() {
        List<ValidationError> validationErrors = validationService.getValidationErrorsForRegisterUser("test123", "ABC123", "1992-12-21");
        assertNotNull(validationErrors);
        assertEquals(1, validationErrors.size());
        assertEquals(PASSWORD_VALIDATION_ERROR.getErrorMessage(), validationErrors.get(0).getErrorMessage());
        assertEquals(PASSWORD_VALIDATION_ERROR.getErrorCode(), validationErrors.get(0).getErrorCode());
    }

    // password with no upper case letter
    @Test
    public void passwordOneUpperCaseFailure() {
        List<ValidationError> validationErrors = validationService.getValidationErrorsForRegisterUser("test123", "abc123", "1992-12-21");
        assertNotNull(validationErrors);
        assertEquals(1, validationErrors.size());
        assertEquals(PASSWORD_VALIDATION_ERROR.getErrorMessage(), validationErrors.get(0).getErrorMessage());
        assertEquals(PASSWORD_VALIDATION_ERROR.getErrorCode(), validationErrors.get(0).getErrorCode());
    }

    // password with no digits
    @Test
    public void passwordWithNoDigitsFailure() {
        List<ValidationError> validationErrors = validationService.getValidationErrorsForRegisterUser("test123", "abcdef", "1992-12-21");
        assertNotNull(validationErrors);
        assertEquals(1, validationErrors.size());
        assertEquals(PASSWORD_VALIDATION_ERROR.getErrorMessage(), validationErrors.get(0).getErrorMessage());
        assertEquals(PASSWORD_VALIDATION_ERROR.getErrorCode(), validationErrors.get(0).getErrorCode());
    }

    // password with no digits
    @Test
    public void passwordMinimumCharacterLengthFailure() {
        List<ValidationError> validationErrors = validationService.getValidationErrorsForRegisterUser("test123", "abc", "1992-12-21");
        assertNotNull(validationErrors);
        assertEquals(1, validationErrors.size());
        assertEquals(PASSWORD_VALIDATION_ERROR.getErrorMessage(), validationErrors.get(0).getErrorMessage());
        assertEquals(PASSWORD_VALIDATION_ERROR.getErrorCode(), validationErrors.get(0).getErrorCode());
    }

    @Test
    public void dateFailure() {
        List<ValidationError> validationErrors = validationService.getValidationErrorsForRegisterUser("test123", "Abc123", "1992-21-21");
        assertNotNull(validationErrors);
        assertEquals(1, validationErrors.size());
        assertEquals(DATE_OF_BIRTH_VALIDATION_ERROR.getErrorMessage(), validationErrors.get(0).getErrorMessage());
        assertEquals(DATE_OF_BIRTH_VALIDATION_ERROR.getErrorCode(), validationErrors.get(0).getErrorCode());
    }

    @Test
    public void multipleFailures() {
        List<ValidationError> validationErrors = validationService.getValidationErrorsForRegisterUser("test 123", "abc123", "1992-21-21");
        assertNotNull(validationErrors);
        assertEquals(3, validationErrors.size());
        assertEquals(USER_NAME_VALIDATION_ERROR.getErrorMessage(), validationErrors.get(0).getErrorMessage());
        assertEquals(USER_NAME_VALIDATION_ERROR.getErrorCode(), validationErrors.get(0).getErrorCode());
        assertEquals(PASSWORD_VALIDATION_ERROR.getErrorMessage(), validationErrors.get(1).getErrorMessage());
        assertEquals(PASSWORD_VALIDATION_ERROR.getErrorCode(), validationErrors.get(1).getErrorCode());
        assertEquals(DATE_OF_BIRTH_VALIDATION_ERROR.getErrorMessage(), validationErrors.get(2).getErrorMessage());
        assertEquals(DATE_OF_BIRTH_VALIDATION_ERROR.getErrorCode(), validationErrors.get(2).getErrorCode());
    }
}
