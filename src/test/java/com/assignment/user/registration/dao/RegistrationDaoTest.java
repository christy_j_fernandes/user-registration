package com.assignment.user.registration.dao;

import com.assignment.user.registration.entities.User;
import com.assignment.user.registration.exceptions.EntityAlreadyExistException;
import com.assignment.user.registration.repositories.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class RegistrationDaoTest {

    @TestConfiguration
    static class RegistrationDaoTestBeanConfiguration {
        @MockBean
        public UserRepository userRepository;

        @Bean
        public RegistrationDao RegistrationDao() {
            return new RegistrationDao(userRepository);
        }
    }

    @Autowired
    public RegistrationDao registrationDao;

    @Autowired
    public UserRepository userRepository;

    @Before
    public void setup() {
        when(userRepository.save(any(User.class))).thenReturn(new User());
        when(userRepository.findById(anyLong())).thenReturn(Optional.of(new User()));
    }

    @Test
    public void createUserTest(){
        registrationDao.createOrUpdateUser(new User());
    }

    @Test(expected = EntityAlreadyExistException.class)
    public void createUserTestException(){
        when(userRepository.save(any(User.class))).thenThrow(DataIntegrityViolationException.class);
        registrationDao.createOrUpdateUser(new User());
    }

    @Test
    public void getUserTest() {
        assertNotNull(registrationDao.getUser(1L));
    }

    @Test(expected = EntityNotFoundException.class)
    public void getUserTestException() {
        when(userRepository.findById(anyLong())).thenThrow(EntityNotFoundException.class);
        registrationDao.getUser(1L);
    }
}
