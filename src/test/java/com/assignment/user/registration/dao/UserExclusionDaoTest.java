package com.assignment.user.registration.dao;

import com.assignment.user.registration.entities.UserExclusion;
import com.assignment.user.registration.exceptions.EntityAlreadyExistException;
import com.assignment.user.registration.repositories.UserExclusionRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class UserExclusionDaoTest {

    @TestConfiguration
    static class UserExclusionDaoTestBeanConfiguration {

        @MockBean
        private UserExclusionRepository userExclusionRepository;

        @Bean
        public UserExclusionDao ExclusionListDao() {
            return new UserExclusionDao(userExclusionRepository);
        }
    }

    @Autowired
    private UserExclusionDao userExclusionDao;

    @Autowired
    private UserExclusionRepository userExclusionRepository;

    @Test
    public void findByDateOfBirthAndSsnTest() {
        when(userExclusionRepository.findByDateOfBirthAndSocialSecurityNumber(any(LocalDate.class), anyString())).thenReturn(new UserExclusion());
        UserExclusion exclusion = userExclusionRepository.findByDateOfBirthAndSocialSecurityNumber(LocalDate.now(), "ssn");
        assertNotNull(exclusion);
    }

    @Test
    public void findByDateOfBirthAndSsnTestNull() {
        when(userExclusionRepository.findByDateOfBirthAndSocialSecurityNumber(any(LocalDate.class), anyString())).thenReturn(null);
        UserExclusion exclusion = userExclusionRepository.findByDateOfBirthAndSocialSecurityNumber(LocalDate.now(), "ssn");
        assertNull(exclusion);
    }

    @Test
    public void testGetAll() {
        List<UserExclusion> userExclusions = asList(new UserExclusion());
        when(userExclusionRepository.findAll()).thenReturn(userExclusions);
        List<UserExclusion> exclusions = userExclusionDao.getAll();
        assertEquals(exclusions, userExclusions);
    }

    @Test
    public void testAddExclusion() {
        when(userExclusionRepository.save(any(UserExclusion.class))).thenReturn(new UserExclusion());
        userExclusionDao.addExclusion(new UserExclusion());
        verify(userExclusionRepository, times(1)).save(any(UserExclusion.class));
    }

    @Test(expected = EntityAlreadyExistException.class)
    public void testAddExclusionException() {
        doThrow(DataIntegrityViolationException.class).when(userExclusionRepository).save(any(UserExclusion.class));
        userExclusionDao.addExclusion(new UserExclusion());
    }

    @Test
    public void testRemoveExclusion() {
        doNothing().when(userExclusionRepository).delete(any(UserExclusion.class));
        userExclusionDao.removeExclusion(new UserExclusion());
        verify(userExclusionRepository, times(1)).delete(any(UserExclusion.class));
    }
}
