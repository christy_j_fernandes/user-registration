package com.assignment.user.registration.controller;

import com.assignment.user.registration.domain.RestResponse;
import com.assignment.user.registration.exceptions.BadRequestException;
import com.assignment.user.registration.exceptions.UserPresentInExclusionListException;
import com.assignment.user.registration.service.RegistrationService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import javax.persistence.EntityNotFoundException;
import java.util.Map;

import static com.assignment.user.registration.domain.ValidationError.USER_NAME_VALIDATION_ERROR;
import static com.assignment.user.registration.domain.ValidationError.USER_PRESENT_IN_EXCLUSION_LIST_VALIDATION_ERROR;
import static com.assignment.user.registration.util.UserRegistrationUtil.buildRestResponse;
import static com.assignment.user.registration.util.UserRegistrationUtil.getErrorDetails;
import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(RegistrationController.class)
public class RegistrationControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private RegistrationService registrationService;

    @Test
    public void registerTest() throws Exception {
        doNothing().when(registrationService).registerUser(anyString(), anyString(), anyString(), anyString());

        MvcResult mvcResult = mvc.perform(post("/user/register").params(getParams())).andReturn();
        String responseJson = mvcResult.getResponse().getContentAsString();
        RestResponse restResponse = new ObjectMapper().readValue(responseJson, RestResponse.class);
        assertNotNull(restResponse);
        assertEquals(CREATED.value(), restResponse.getStatus());
    }

    @Test
    public void registerTestValidationFailure() throws Exception {
        doThrow(new BadRequestException(asList(USER_NAME_VALIDATION_ERROR)))
                .when(registrationService).registerUser(anyString(), anyString(), anyString(), anyString());

        ResultActions resultActions = mvc.perform(post("/user/register").params(getParams()));
        resultActions.andExpect(status().is(BAD_REQUEST.value()));
        Map<String, String> errorDetails = getErrorDetails(USER_NAME_VALIDATION_ERROR);
        RestResponse restResponse = buildRestResponse(BAD_REQUEST.value(), BAD_REQUEST.getReasonPhrase(), asList(errorDetails));
        resultActions.andExpect(content().json(new ObjectMapper().writeValueAsString(restResponse)));
    }

    @Test
    public void registerTestExclusionFailure() throws Exception {
        doThrow(new UserPresentInExclusionListException(USER_PRESENT_IN_EXCLUSION_LIST_VALIDATION_ERROR))
                .when(registrationService).registerUser(anyString(), anyString(), anyString(), anyString());

        ResultActions resultActions = mvc.perform(post("/user/register").params(getParams()));
        resultActions.andExpect(status().is(FORBIDDEN.value()));
        Map<String, String> errorDetails = getErrorDetails(USER_PRESENT_IN_EXCLUSION_LIST_VALIDATION_ERROR);
        RestResponse restResponse = buildRestResponse(FORBIDDEN.value(), FORBIDDEN.getReasonPhrase(), asList(errorDetails));
        resultActions.andExpect(content().json((new ObjectMapper().writeValueAsString(restResponse))));
    }

    @Test
    public void deactivateUserTest() throws Exception {
        doNothing().when(registrationService).deactivateUser(new Long(1));

        ResultActions resultActions = mvc.perform(put("/user/1/deactivate").params(getParams()));
        resultActions.andExpect(status().is(HttpStatus.OK.value()));
    }

    @Test
    public void deactivateUserTestException() throws Exception {
        doThrow(new EntityNotFoundException())
                .when(registrationService).deactivateUser(new Long(1));

        ResultActions resultActions = mvc.perform(put("/user/1/deactivate").params(getParams()));
        resultActions.andExpect(status().is(HttpStatus.NOT_FOUND.value()));
    }

    private MultiValueMap<String, String> getParams() {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("username", "abc");
        params.add("password", "Abc1234");
        params.add("dateOfBirth", "1992-12-21");
        params.add("ssn", "1234567");
        return params;
    }
}
