package com.assignment.user.registration.controller;

import com.assignment.user.registration.domain.RestResponse;
import com.assignment.user.registration.entities.UserExclusion;
import com.assignment.user.registration.service.ExclusionService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import javax.persistence.EntityNotFoundException;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(UserExclusionController.class)
public class UserExclusionControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ExclusionService exclusionService;

    @Test
    public void testAllGet() throws Exception {
        List<UserExclusion> exclusions = asList(new UserExclusion());
        when(exclusionService.getAll()).thenReturn(exclusions);
        ResultActions resultActions = mvc.perform(get("/exclusion/all"));
        resultActions.andExpect(status().is(HttpStatus.OK.value()));
    }

    @Test
    public void testAddExclusion() throws Exception {
        doNothing().when(exclusionService).addExclusion(anyString(), anyString());

        MvcResult mvcResult = mvc.perform(post("/exclusion/add").params(getParams())).andReturn();
        String responseJson = mvcResult.getResponse().getContentAsString();
        RestResponse restResponse = new ObjectMapper().readValue(responseJson, RestResponse.class);
        assertNotNull(restResponse);
        assertEquals(CREATED.value(), restResponse.getStatus());
    }

    @Test
    public void testRemoveExclusion() throws Exception {
        doNothing().when(exclusionService).removeExclusion(anyString(), anyString());

        ResultActions resultActions = mvc.perform(put("/exclusion/remove").params(getParams()));
        resultActions.andExpect(status().is(HttpStatus.OK.value()));
    }

    @Test
    public void testRemoveExclusionException() throws Exception {
        doThrow(EntityNotFoundException.class).when(exclusionService).removeExclusion(anyString(), anyString());

        ResultActions resultActions = mvc.perform(put("/exclusion/remove").params(getParams()));
        resultActions.andExpect(status().is(HttpStatus.NOT_FOUND.value()));
    }


    private MultiValueMap<String, String> getParams() {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("dateOfBirth", "1992-12-21");
        params.add("ssn", "1234567");
        return params;
    }
}
