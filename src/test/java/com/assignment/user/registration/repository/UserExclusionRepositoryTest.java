package com.assignment.user.registration.repository;

import com.assignment.user.registration.entities.UserExclusion;
import com.assignment.user.registration.repositories.UserExclusionRepository;
import org.apache.commons.collections4.IteratorUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.List;

import static com.assignment.user.registration.util.UserRegistrationUtil.buildExclusion;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserExclusionRepositoryTest {

    @Autowired
    private UserExclusionRepository userExclusionRepository;

    @Test
    public void testFindByDateOfBirthAndSsn() {
        LocalDate date = LocalDate.of(1912, 06, 23);
        String ssn = "123456789";
        UserExclusion byDateOfBirthAndSsn = userExclusionRepository.findByDateOfBirthAndSocialSecurityNumber(date, ssn);
        assertNotNull(byDateOfBirthAndSsn);
        assertEquals(new Long(2), byDateOfBirthAndSsn.getId());
        assertEquals(date, byDateOfBirthAndSsn.getDateOfBirth());
        assertEquals(ssn, byDateOfBirthAndSsn.getSocialSecurityNumber());
    }

    @Test
    public void testFindByDateOfBirthAndSsnNotPresent() {
        LocalDate date = LocalDate.of(1912, 06, 21);
        String ssn = "123456789";
        UserExclusion byDateOfBirthAndSsn = userExclusionRepository.findByDateOfBirthAndSocialSecurityNumber(date, ssn);
        assertNull(byDateOfBirthAndSsn);
    }

    @Test
    public void testGetAllExclusion() {
        fetchAndPerformAssert(3);
    }

    @Test
    public void testAddExclusion() {
        String dob = "1992-12-21";
        LocalDate dateOfBirth = LocalDate.of(1992, 12, 21);
        String ssn = "ssn";
        fetchAndPerformAssert(3);
        userExclusionRepository.save(buildExclusion(dob, ssn));
        List<UserExclusion> exclusions = fetchAndPerformAssert(4);
        assertEquals(dateOfBirth, exclusions.get(3).getDateOfBirth());
        assertEquals(ssn, exclusions.get(3).getSocialSecurityNumber());
    }

    @Test
    public void testRemoveExclusion() {
        LocalDate date = LocalDate.of(1912, 06, 23);
        String ssn = "123456789";
        UserExclusion exclusion = userExclusionRepository.findByDateOfBirthAndSocialSecurityNumber(date, ssn);
        assertNotNull(exclusion);
        userExclusionRepository.delete(exclusion);
        exclusion = userExclusionRepository.findByDateOfBirthAndSocialSecurityNumber(exclusion.getDateOfBirth(), exclusion.getSocialSecurityNumber());
        assertNull(exclusion);
    }

    private List<UserExclusion> fetchAndPerformAssert(int count) {
        Iterable<UserExclusion> exclusions = userExclusionRepository.findAll();
        assertNotNull(exclusions);
        List<UserExclusion> list = IteratorUtils.toList(exclusions.iterator());
        assertEquals(count, list.size());
        return list;
    }
}
