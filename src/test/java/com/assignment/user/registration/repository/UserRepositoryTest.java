package com.assignment.user.registration.repository;

import com.assignment.user.registration.entities.User;
import com.assignment.user.registration.repositories.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;

import static com.assignment.user.registration.util.UserRegistrationUtil.buildUser;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void createUserTest() {
        Iterable<User> users = userRepository.findAll();
        assertFalse(users.iterator().hasNext());

        String userName = "userName";
        String password = "password";
        String dob = "1992-12-21";
        String ssn = "ssn";
        LocalDate regDate = LocalDate.now();

        User user = buildUser(userName, password, dob,ssn, regDate, true);

        userRepository.save(user);

        users = userRepository.findAll();
        assertTrue(users.iterator().hasNext());
        assertEquals(user, users.iterator().next());
    }
}
