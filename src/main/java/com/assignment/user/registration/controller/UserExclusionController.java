package com.assignment.user.registration.controller;

import com.assignment.user.registration.domain.RestResponse;
import com.assignment.user.registration.entities.UserExclusion;
import com.assignment.user.registration.service.ExclusionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.assignment.user.registration.domain.ApplicationConstants.*;
import static com.assignment.user.registration.util.UserRegistrationUtil.buildRestResponse;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

/**
 * Controller for fetching/adding/removing user exclusions
 */
@RestController
@RequestMapping("/exclusion")
public class UserExclusionController {

    private ExclusionService exclusionService;

    @Autowired
    public UserExclusionController(ExclusionService exclusionService) {
        this.exclusionService = exclusionService;
    }

    /**
     * Fetch all user exclusions
     * Success status code: 200
     *
     * @return
     */
    @GetMapping("/all")
    public RestResponse getAllExclusions(){
        List<UserExclusion> exclusions = exclusionService.getAll();
        return buildRestResponse(OK.value(), OK.getReasonPhrase(), USER_EXCLUSIONS_FETCHED, exclusions);
    }

    /**
     * Add a new user exclusion
     * Success status code: 201 (CREATED)
     * Returns 400 (Bad Request) if Entity already exist
     *
     * @param dateOfBirth
     * @param ssn
     * @return
     */
    @PostMapping("/add")
    public RestResponse addExclusion(@RequestParam("dateOfBirth") String dateOfBirth, @RequestParam("ssn") String ssn){
        exclusionService.addExclusion(dateOfBirth, ssn);
        return buildRestResponse(CREATED.value(), CREATED.getReasonPhrase(), USER_EXCLUSIONS_ADDED);
    }

    /**
     * Remove a user exclusion
     * Success status code: 200 (OK)
     * Returns 404 (Entity Not found) if the user exclusion to be removed doesn't exist
     *
     * @param dateOfBirth
     * @param ssn
     * @return
     */
    @PutMapping("/remove")
    public RestResponse removeExclusion(@RequestParam("dateOfBirth") String dateOfBirth, @RequestParam("ssn") String ssn){
        exclusionService.removeExclusion(dateOfBirth, ssn);
        return buildRestResponse(OK.value(), OK.getReasonPhrase(), USER_EXCLUSIONS_REMOVED);
    }

}
