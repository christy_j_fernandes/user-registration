package com.assignment.user.registration.controller;

import com.assignment.user.registration.domain.RestResponse;
import com.assignment.user.registration.service.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static com.assignment.user.registration.domain.ApplicationConstants.ENTITY_CREATED;
import static com.assignment.user.registration.domain.ApplicationConstants.USER_DEACTIVATED;
import static com.assignment.user.registration.util.UserRegistrationUtil.buildRestResponse;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

/**
 * Controller for user registration and deactivation
 */
@RestController
@RequestMapping("/user")
public class RegistrationController {

    private RegistrationService registrationService;

    @Autowired
    public RegistrationController(RegistrationService registrationService) {
        this.registrationService = registrationService;
    }

    /**
     * Register a user
     * Success status code: 201 (CREATED)
     * Returns 400 (Bad Request) if Entity already exist
     *
     * @param userName
     * @param password
     * @param dateOfBirth
     * @param ssn
     * @return
     */
    @PostMapping("/register")
    public RestResponse register(@RequestParam("username") String userName, @RequestParam("password") String password,
                                 @RequestParam("dateOfBirth") String dateOfBirth, @RequestParam("ssn") String ssn) {
        registrationService.registerUser(userName, password, dateOfBirth, ssn);
        return buildRestResponse(CREATED.value(), CREATED.getReasonPhrase(), ENTITY_CREATED);
    }

    /**
     * Deactivate a user
     * Success status code: 200 (OK)
     * Returns 404 (Entity Not found) if the user exclusion to be removed doesn't exist
     *
     * @param id
     * @return
     */
    @PutMapping("/{id}/deactivate")
    public RestResponse deactivateUser(@PathVariable("id") long id) {
        registrationService.deactivateUser(id);
        return buildRestResponse(OK.value(), OK.getReasonPhrase(), USER_DEACTIVATED);
    }
}
