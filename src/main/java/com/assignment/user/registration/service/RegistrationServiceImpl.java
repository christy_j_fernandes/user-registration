package com.assignment.user.registration.service;

import com.assignment.user.registration.dao.RegistrationDao;
import com.assignment.user.registration.domain.ValidationError;
import com.assignment.user.registration.entities.User;
import com.assignment.user.registration.exceptions.BadRequestException;
import com.assignment.user.registration.exceptions.UserPresentInExclusionListException;
import com.assignment.user.registration.service.validation.ValidationService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

import static com.assignment.user.registration.domain.ValidationError.USER_PRESENT_IN_EXCLUSION_LIST_VALIDATION_ERROR;
import static com.assignment.user.registration.util.UserRegistrationUtil.buildUser;
import static java.util.Objects.nonNull;

@Service
public class RegistrationServiceImpl implements RegistrationService {

    private RegistrationDao registrationDao;
    private ExclusionService exclusionService;
    private ValidationService validationService;

    @Autowired
    public RegistrationServiceImpl(RegistrationDao registrationDao, ExclusionService exclusionService, ValidationService validationService){
        this.registrationDao = registrationDao;
        this.exclusionService = exclusionService;
        this.validationService = validationService;
    }

    private static final Log LOG = LogFactory.getLog(RegistrationServiceImpl.class);

    /**
     * Registers a new user
     * Throws Bad Request if validation failures
     * Throws UserPresentInExclusionListException if user present in User Exclusion list
     *
     * @param userName
     * @param password
     * @param dateOfBirth
     * @param ssn
     */
    @Override
    public void registerUser(String userName, String password, String dateOfBirth, String  ssn) {
        List<ValidationError> validationErrors = validationService.getValidationErrorsForRegisterUser(userName, password, dateOfBirth);
        if(nonNull(validationErrors)) {
            LOG.error("Validation Errors while registering a user :: " + validationErrors.toString());
            throw new BadRequestException(validationErrors);
        }

        boolean isExcluded = exclusionService.validate(dateOfBirth, ssn);
        if(isExcluded) {
            LOG.error("User present in the exclusion list");
            throw new UserPresentInExclusionListException(USER_PRESENT_IN_EXCLUSION_LIST_VALIDATION_ERROR);
        }

        User user = buildUser(userName, password, dateOfBirth, ssn, LocalDate.now(), true);
        registrationDao.createOrUpdateUser(user);
        LOG.info("User registered successfully");
    }

    /**
     * Deactivates a user
     *
     * @param id
     */
    @Override
    public void deactivateUser(Long id) {
        User user = registrationDao.getUser(id);
        user.setActive(false);
        registrationDao.createOrUpdateUser(user);
        LOG.info("User deactivated successfully");
    }
}
