package com.assignment.user.registration.service;

import com.assignment.user.registration.dao.UserExclusionDao;
import com.assignment.user.registration.domain.ValidationError;
import com.assignment.user.registration.entities.UserExclusion;
import com.assignment.user.registration.exceptions.BadRequestException;
import com.assignment.user.registration.service.validation.ValidationService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

import static com.assignment.user.registration.util.UserRegistrationUtil.buildExclusion;
import static com.assignment.user.registration.util.UserRegistrationUtil.parseDate;
import static java.util.Arrays.asList;
import static java.util.Objects.nonNull;

@Service
public class ExclusionServiceImpl implements ExclusionService {

    private ValidationService validationService;
    private UserExclusionDao exclusionDao;

    @Autowired
    public ExclusionServiceImpl(UserExclusionDao exclusionDao, ValidationService validationService) {
        this.exclusionDao = exclusionDao;
        this.validationService = validationService;
    }

    private static final Log LOG = LogFactory.getLog(ExclusionServiceImpl.class);

    /**
     * Get all user exclusions
     *
     * @return
     */
    @Override
    public List<UserExclusion> getAll() {
        return exclusionDao.getAll();
    }

    /**
     * Get a user exclusion
     *
     * @param dateOfBirth
     * @param ssn
     * @return
     */
    @Override
    public UserExclusion getExclusion(String dateOfBirth, String ssn) {
        performDateOfBirthValidation(dateOfBirth);
        return exclusionDao.findByDateOfBirthAndSsn(parseDate(dateOfBirth), ssn);
    }

    /**
     * Add a new exclusion
     *
     * @param dateOfBirth
     * @param ssn
     */
    @Override
    public void addExclusion(String dateOfBirth, String ssn) {
        performDateOfBirthValidation(dateOfBirth);
        UserExclusion exclusion = buildExclusion(dateOfBirth, ssn);
        exclusionDao.addExclusion(exclusion);
        LOG.info("User exclusion added successfully");
    }

    /**
     * remove a user exclusion
     *
     * @param dateOfBirth
     * @param ssn
     */
    @Override
    public void removeExclusion(String dateOfBirth, String ssn) {
        performDateOfBirthValidation(dateOfBirth);
        UserExclusion exclusion = getExclusion(dateOfBirth, ssn);
        exclusionDao.removeExclusion(exclusion);
        LOG.info("User exclusion removed successfully");
    }

    /**
     * Checks if a user is present in the user exclusion list
     *
     * @param dateOfBirth
     * @param ssn
     * @return
     */
    @Override
    public boolean validate(String dateOfBirth, String ssn) {
        try {
            getExclusion(dateOfBirth, ssn);
        } catch (EntityNotFoundException e) {
            return false;
        }
        return true;
    }

    private void performDateOfBirthValidation(String dateOfBirth) {
        ValidationError validationError = validationService.validateDateOfBirth(dateOfBirth);
        if(nonNull(validationError)) {
            LOG.error("Invalid date :: "+ dateOfBirth);
            throw new BadRequestException(asList(validationError));
        }
    }
}
