package com.assignment.user.registration.service;

import com.assignment.user.registration.entities.UserExclusion;

import java.util.List;

public interface ExclusionService {

    public List<UserExclusion> getAll();

    public UserExclusion getExclusion(String dateOfBirth, String ssn);

    public void addExclusion(String dateOfBirth, String ssn);

    public void removeExclusion(String dateOfBirth, String ssn);

    public boolean validate(String dateOfBirth, String ssn);
}
