package com.assignment.user.registration.service.validation;

import com.assignment.user.registration.domain.ValidationError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.DateTimeException;
import java.util.ArrayList;
import java.util.List;

import static com.assignment.user.registration.domain.ApplicationConstants.DEFAULT_PASSWORD_REGEX;
import static com.assignment.user.registration.domain.ApplicationConstants.DEFAULT_USER_NAME_REGEX;
import static com.assignment.user.registration.domain.ValidationError.*;
import static com.assignment.user.registration.util.UserRegistrationUtil.parseDate;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static org.apache.commons.lang3.StringUtils.isBlank;

@Service
public class ValidationServiceImpl implements ValidationService {

    public String userNameRegex, passwordRegex;

    @Autowired
    public ValidationServiceImpl(@Value("${username.validation.regex:" + DEFAULT_USER_NAME_REGEX + "}") String userNameRegex,
                                 @Value("${password.validation.regex:" + DEFAULT_PASSWORD_REGEX + "}") String passwordRegex) {
        this.userNameRegex = userNameRegex;
        this.passwordRegex = passwordRegex;
    }

    /**
     * Validates username, password and dateOfBirth for a user
     *
     * UserName validation: Alphanumeric only without space
     * Password validation: (1) One small case character (2) One upper case character (3) One digit (4) Minimum 4 characters
     * Date of birth validation: Checks if a valid date
     *
     * @param userName
     * @param password
     * @param dateOfBirth
     * @return
     */
    @Override
    public List<ValidationError> getValidationErrorsForRegisterUser(String userName, String password, String dateOfBirth) {
        List<ValidationError> errorMessages = null;

        if(isBlank(userName) || !isUserNameValid(userName)) {
            errorMessages = new ArrayList<>();
            errorMessages.add(USER_NAME_VALIDATION_ERROR);
        }

        if(isBlank(password) || !isPasswordValid(password)) {
            errorMessages = isNull(errorMessages) ? new ArrayList<>() : errorMessages;
            errorMessages.add(PASSWORD_VALIDATION_ERROR);
        }

        ValidationError dobValidationError = validateDateOfBirth(dateOfBirth);

        if(nonNull(dobValidationError)) {
            errorMessages = isNull(errorMessages) ? new ArrayList<>() : errorMessages;
            errorMessages.add(DATE_OF_BIRTH_VALIDATION_ERROR);
        }

        return errorMessages;
    }

    /**
     * Validates date of birth (if valid date)
     *
     * @param dateOfBirth
     * @return
     */
    @Override
    public ValidationError validateDateOfBirth(String dateOfBirth) {
        try {
            parseDate(dateOfBirth);
        } catch (DateTimeException | NullPointerException e) {
            return DATE_OF_BIRTH_VALIDATION_ERROR;
        }
        return null;
    }

    private boolean isUserNameValid(String userName) {
        return userName.matches(userNameRegex);
    }

    private boolean isPasswordValid(String password) {
        return password.matches(passwordRegex);
    }

}
