package com.assignment.user.registration.service.validation;

import com.assignment.user.registration.domain.ValidationError;

import java.util.List;

public interface ValidationService {

    public List<ValidationError> getValidationErrorsForRegisterUser(String userName, String password, String ssn);

    public ValidationError validateDateOfBirth(String dateOfBirth);

}
