package com.assignment.user.registration.service;

public interface RegistrationService {

    public void registerUser(String userName, String password, String dateOfBirth, String ssn);

    public void deactivateUser(Long id);
}
