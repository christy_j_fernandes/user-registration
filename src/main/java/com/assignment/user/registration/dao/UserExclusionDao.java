package com.assignment.user.registration.dao;

import com.assignment.user.registration.entities.UserExclusion;
import com.assignment.user.registration.exceptions.EntityAlreadyExistException;
import com.assignment.user.registration.repositories.UserExclusionRepository;
import org.apache.commons.collections4.IteratorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.util.List;

import static com.assignment.user.registration.domain.ValidationError.EXCLUSION_ALREADY_EXIST_VALIDATION_ERROR;
import static java.util.Objects.nonNull;

@Component
public class UserExclusionDao {

    private UserExclusionRepository userExclusionRepository;

    @Autowired
    public UserExclusionDao(UserExclusionRepository userExclusionRepository) {
        this.userExclusionRepository = userExclusionRepository;
    }

    /**
     * Fetches user exclusion based on Date of Birth and Social Security Number
     * Throws EntityNotFoundException if entity not found
     *
     * @param dateOfBirth
     * @param ssn
     * @return
     */
    public UserExclusion findByDateOfBirthAndSsn(LocalDate dateOfBirth, String ssn) {
        UserExclusion exclusion = userExclusionRepository.findByDateOfBirthAndSocialSecurityNumber(dateOfBirth, ssn);
        if (nonNull(exclusion)) {
            return exclusion;
        }
        return getAll().stream()
                .filter(e -> dateOfBirth.isEqual(e.getDateOfBirth()) && ssn.equals(e.getSocialSecurityNumber()))
                .findFirst()
                .orElseThrow(EntityNotFoundException::new);
    }

    /**
     * Fetches all user exclusions
     *
     * @return
     */
    public List<UserExclusion> getAll() {
        Iterable<UserExclusion> exclusions = userExclusionRepository.findAll();
        return IteratorUtils.toList(exclusions.iterator());
    }

    /**
     * Adds a new user exclusion
     * Throws EntityAlreadyExistException if user exclusion already present
     *
     * @param userExclusion
     */
    public void addExclusion(UserExclusion userExclusion) {
        try {
            userExclusionRepository.save(userExclusion);
        } catch (DataIntegrityViolationException e) {
            throw new EntityAlreadyExistException(EXCLUSION_ALREADY_EXIST_VALIDATION_ERROR);
        }
    }

    /**
     * Removes a user exclusion
     *
     * @param userExclusion
     */
    public void removeExclusion(UserExclusion userExclusion) {
        userExclusionRepository.delete(userExclusion);
    }
}
