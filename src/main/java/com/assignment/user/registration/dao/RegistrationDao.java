package com.assignment.user.registration.dao;

import com.assignment.user.registration.entities.User;
import com.assignment.user.registration.exceptions.EntityAlreadyExistException;
import com.assignment.user.registration.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;

import static com.assignment.user.registration.domain.ValidationError.USER_ALREADY_EXIST_VALIDATION_ERROR;

@Component
public class RegistrationDao {

    private UserRepository userRepository;

    @Autowired
    public RegistrationDao(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    /**
     * Get user using Id
     *
     * @param id
     * @return
     */
    public User getUser(long id) {
        return userRepository.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    /**
     * Create or Update a user
     * Throws EntityAlreadyExistException if user already present
     *
     * @param user
     */
    public void createOrUpdateUser(User user) {
        try {
            userRepository.save(user);
        } catch (DataIntegrityViolationException e) {
            throw new EntityAlreadyExistException(USER_ALREADY_EXIST_VALIDATION_ERROR);
        }
    }
}
