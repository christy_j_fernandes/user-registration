package com.assignment.user.registration.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Entity
@Table(name = "USER_EXCLUSION")
public class UserExclusion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "DATE_OF_BIRTH", columnDefinition = "DATE", nullable = false)
    private LocalDate dateOfBirth;

    @Column(name = "SOCIAL_SECURITY_NUMBER", nullable = false, unique = true)
    private String socialSecurityNumber;
}
