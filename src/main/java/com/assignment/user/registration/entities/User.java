package com.assignment.user.registration.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Entity
@Table(name = "USER")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "USER_NAME", nullable = false, unique = true)
    private String userName;

    @Column(name = "PASSWORD", nullable = false)
    private String password;

    @Column(name = "DATE_OF_BIRTH", columnDefinition = "DATE", nullable = false)
    @CreatedDate
    private LocalDate dateOfBirth;

    @Column(name = "SOCIAL_SECURITY_NUMBER", nullable = false, unique = true)
    private String socialSecurityNumber;

    @Column(name = "REGISTRATION_DATE", columnDefinition = "DATE", nullable = false)
    @CreatedDate
    private LocalDate registrationDate;

    @Column(name = "IS_ACTIVE", columnDefinition="BOOLEAN DEFAULT true", nullable = false)
    private boolean isActive;
}
