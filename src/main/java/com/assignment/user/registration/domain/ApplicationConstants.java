package com.assignment.user.registration.domain;

public interface ApplicationConstants {

    String ENTITY_CREATED = "Entity Created";
    String USER_DEACTIVATED = "User Deactivated";
    String USER_EXCLUSIONS_FETCHED = "All user exclusions fetched";
    String USER_EXCLUSIONS_ADDED = "User exclusion added";
    String USER_EXCLUSIONS_REMOVED = "User exclusion removed";
    String ERROR_CODE = "error_code";
    String ERROR_MESSAGE = "error_message";
    String DEFAULT_USER_NAME_REGEX = "^[\\p{Alnum}]+$";
    String DEFAULT_PASSWORD_REGEX = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s).{4,}$";
}
