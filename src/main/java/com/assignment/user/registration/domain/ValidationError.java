package com.assignment.user.registration.domain;

public enum ValidationError {

    USER_NAME_VALIDATION_ERROR("username.validation.error", "Invalid user name :: Only alphanumeric without space and any special symbols"),
    PASSWORD_VALIDATION_ERROR("password.validation.error", "Invalid password :: Ensure password has (1) One small case character " +
            "(2) One upper case character (3) One digit (4) Minimum 4 characters"),
    DATE_OF_BIRTH_VALIDATION_ERROR("dateOfBirth.validation.error", "Invalid date of birth"),
    USER_PRESENT_IN_EXCLUSION_LIST_VALIDATION_ERROR("user.present.in.exclusion.list", "User present in the exclusion list"),
    USER_ALREADY_EXIST_VALIDATION_ERROR("user.already.present", "User already present"),
    EXCLUSION_ALREADY_EXIST_VALIDATION_ERROR("exclusion.already.present", "User exclusion already present"),
    ENTITY_NOT_FOUND("entity.not.found", "Entity not found"),
    PARAMETER_MISSING("parameter.missing", "Parameter Missing : %s");

    private final String errorCode;
    private final String errorMessage;

    ValidationError(String errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

}
