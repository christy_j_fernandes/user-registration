package com.assignment.user.registration.configurations;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 *  Persistence configuration to enable JPA for Spring Data
 *  Added separately so that it is not invoked with the main Spring Boot application class.
 */
@Configuration
@EntityScan("com.assignment.user.registration.entities")
@EnableJpaRepositories("com.assignment.user.registration.repositories")
public class PersistenceConfiguration {
}
