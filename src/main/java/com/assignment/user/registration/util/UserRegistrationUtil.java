package com.assignment.user.registration.util;

import com.assignment.user.registration.domain.RestResponse;
import com.assignment.user.registration.domain.ValidationError;
import com.assignment.user.registration.entities.User;
import com.assignment.user.registration.entities.UserExclusion;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.assignment.user.registration.domain.ApplicationConstants.ERROR_CODE;
import static com.assignment.user.registration.domain.ApplicationConstants.ERROR_MESSAGE;

public class UserRegistrationUtil {

    public static User buildUser(String userName, String password, String dateOfBirth, String ssn, LocalDate registrationDate, boolean isActive){
        return User.builder()
                .userName(userName)
                .password(password)
                .dateOfBirth(parseDate(dateOfBirth))
                .socialSecurityNumber(ssn)
                .registrationDate(registrationDate)
                .isActive(isActive)
                .build();
    }

    public static UserExclusion buildExclusion(String dateOfBirth, String ssn) {
        return UserExclusion.builder()
                .dateOfBirth(parseDate(dateOfBirth))
                .socialSecurityNumber(ssn)
                .build();
    }

    public static RestResponse buildRestResponse(int status, String statusDescription, List<Map<String, String>> errorDetails) {
        return buildRestResponse(status, statusDescription, null, null, errorDetails);
    }

    public static RestResponse buildRestResponse(int status, String statusDescription, String message) {
        return buildRestResponse(status, statusDescription, message, null, null);
    }

    public static RestResponse buildRestResponse(int status, String statusDescription, String message, Object content) {
        return buildRestResponse(status, statusDescription, message, content, null);
    }

    public static RestResponse buildRestResponse(int status, String statusDescription, String message, Object content, List<Map<String, String>> errorDetails) {
        return RestResponse.builder()
                .status(status)
                .statusDescription(statusDescription)
                .message(message)
                .content(content)
                .errorDetails(errorDetails)
                .build();
    }

    public static Map<String, String> getErrorDetails(ValidationError validationError) {
        return new HashMap<String, String>() {
            { put(ERROR_MESSAGE, validationError.getErrorMessage()); }
            { put(ERROR_CODE, validationError.getErrorCode()); }
        };
    }

    public static LocalDate parseDate(String dateOfBirth){
        return LocalDate.from(DateTimeFormatter.ISO_LOCAL_DATE.parse(dateOfBirth));
    }

}
