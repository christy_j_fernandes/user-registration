package com.assignment.user.registration.repositories;

import com.assignment.user.registration.entities.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
}
