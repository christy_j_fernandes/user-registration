package com.assignment.user.registration.repositories;

import com.assignment.user.registration.entities.UserExclusion;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;

public interface UserExclusionRepository extends CrudRepository<UserExclusion, Long> {

    /**
     * To fetch User exclusion using Date of birth and Social Security Number
     *
     * @param dateOfBirth
     * @param socialSecurityNumber
     * @return
     */
    @Query("Select e from UserExclusion e WHERE e.dateOfBirth = ?1 and e.socialSecurityNumber = ?2")
    public UserExclusion findByDateOfBirthAndSocialSecurityNumber(LocalDate dateOfBirth, String socialSecurityNumber);
}
