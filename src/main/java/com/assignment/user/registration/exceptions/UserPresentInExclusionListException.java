package com.assignment.user.registration.exceptions;

import com.assignment.user.registration.domain.ValidationError;
import com.assignment.user.registration.util.UserRegistrationUtil;
import org.springframework.http.HttpStatus;

import java.util.Map;

public class UserPresentInExclusionListException extends RuntimeException {

    ValidationError validationError;

    public UserPresentInExclusionListException(ValidationError validationError) {
        this.validationError = validationError;
    }

    public Map<String, String> getErrorDetails() {
        return UserRegistrationUtil.getErrorDetails(validationError);
    }

    public HttpStatus getStatus() {
        return HttpStatus.FORBIDDEN;
    }

}
