package com.assignment.user.registration.exceptions;

import com.assignment.user.registration.domain.ValidationError;
import com.assignment.user.registration.util.UserRegistrationUtil;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BadRequestException extends RuntimeException {

    List<ValidationError> validationErrors;

    public BadRequestException(List<ValidationError> validationErrors) {
        this.validationErrors = validationErrors;
    }

    public List<Map<String, String>> getErrorDetails() {
        List<Map<String, String>> errorDetails = new ArrayList<>();
        validationErrors.forEach(validation -> {
            Map<String, String> errorDetail = UserRegistrationUtil.getErrorDetails(validation);
            errorDetails.add(errorDetail);
        });
        return errorDetails;
    }

    public HttpStatus getStatus() {
        return HttpStatus.BAD_REQUEST;
    }
}
