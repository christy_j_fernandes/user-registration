package com.assignment.user.registration.exceptions;

import com.assignment.user.registration.domain.RestResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Map;

import static com.assignment.user.registration.domain.ApplicationConstants.ERROR_MESSAGE;
import static com.assignment.user.registration.domain.ValidationError.ENTITY_NOT_FOUND;
import static com.assignment.user.registration.domain.ValidationError.PARAMETER_MISSING;
import static com.assignment.user.registration.util.UserRegistrationUtil.buildRestResponse;
import static com.assignment.user.registration.util.UserRegistrationUtil.getErrorDetails;
import static java.util.Arrays.asList;

@ControllerAdvice
public class UserRegistrationExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(BadRequestException.class)
    protected ResponseEntity<Object> handleBadRequest(
            Exception ex, WebRequest request) {
        HttpStatus status = ((BadRequestException) ex).getStatus();
        List<Map<String, String>> errorDetails = ((BadRequestException) ex).getErrorDetails();
        RestResponse error = buildRestResponse(status.value(), status.getReasonPhrase(), errorDetails);
        return handleExceptionInternal(ex, error, new HttpHeaders(), status, request);
    }

    @ExceptionHandler(UserPresentInExclusionListException.class)
    protected ResponseEntity<Object> handleExclusionException(
            Exception ex, WebRequest request) {
        HttpStatus status = ((UserPresentInExclusionListException) ex).getStatus();
        Map<String, String> errorDetails = ((UserPresentInExclusionListException) ex).getErrorDetails();
        RestResponse error = buildRestResponse(status.value(), status.getReasonPhrase(), asList(errorDetails));
        return handleExceptionInternal(ex, error, new HttpHeaders(), status, request);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    protected ResponseEntity<Object> handleEntityNotFound(
            Exception ex, WebRequest request) {
        HttpStatus status = HttpStatus.NOT_FOUND;
        Map<String, String> errorDetails = getErrorDetails(ENTITY_NOT_FOUND);
        RestResponse error = buildRestResponse(status.value(), status.getReasonPhrase(), asList(errorDetails));
        return handleExceptionInternal(ex, error, new HttpHeaders(), status, request);
    }

    @ExceptionHandler(EntityAlreadyExistException.class)
    protected ResponseEntity<Object> handleEntityAlreadyExist(
            Exception ex, WebRequest request) {
        HttpStatus status = ((EntityAlreadyExistException) ex).getStatus();
        Map<String, String> errorDetails = ((EntityAlreadyExistException) ex).getErrorDetails();
        RestResponse error = buildRestResponse(status.value(), status.getReasonPhrase(), asList(errorDetails));
        return handleExceptionInternal(ex, error, new HttpHeaders(), status, request);
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex,
            HttpHeaders headers, HttpStatus status, WebRequest request) {
        String name = ex.getParameterName();
        Map<String, String> errorDetails = getErrorDetails(PARAMETER_MISSING);
        errorDetails.put(ERROR_MESSAGE, String.format(errorDetails.get(ERROR_MESSAGE), name));
        RestResponse error = buildRestResponse(status.value(), status.getReasonPhrase(), asList(errorDetails));
        return handleExceptionInternal(ex, error, new HttpHeaders(), status, request);
    }
}
