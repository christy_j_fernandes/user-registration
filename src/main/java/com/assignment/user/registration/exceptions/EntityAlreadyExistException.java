package com.assignment.user.registration.exceptions;

import com.assignment.user.registration.domain.ValidationError;
import com.assignment.user.registration.util.UserRegistrationUtil;
import org.springframework.http.HttpStatus;

import java.util.Map;

public class EntityAlreadyExistException extends RuntimeException {

    ValidationError validationError;

    public EntityAlreadyExistException(ValidationError validationError) {
        this.validationError = validationError;
    }

    public Map<String, String> getErrorDetails() {
        return UserRegistrationUtil.getErrorDetails(validationError);
    }

    public HttpStatus getStatus() {
        return HttpStatus.BAD_REQUEST;
    }
}
