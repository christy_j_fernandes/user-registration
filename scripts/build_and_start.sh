######### VARS #################
JAR_FILE=target/user-registration-0.0.1-SNAPSHOT.jar
#################################

# first build the package
cd ../
echo "##############################"
echo "# Application build statring #"
echo "##############################"

# mvn clean package

echo
if [ ! -f "$JAR_FILE" ]
then
        echo "############################################################"
        echo "# ERROR : Application build failed, jar file not generated #"
        echo "############################################################"
else
        echo "###################################################"
        echo "# Application build done, starting application... #"
        echo "###################################################"
        echo
        # start the jar
        java -jar "$JAR_FILE"
fi