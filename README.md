#User Registration

- This application provides endpoint for user to register themselves.
- There is an exclusion list, which forbids certain user from registering themselves. 
- Exclusion is based on Date Of Birth & Social Security Number

---

## Frameworks used

1.	Spring Boot
2.	H2 (In Memory database)
3.	Liquibase to create and pre-populate User Exclusion table
4.	Persistence: Spring Data JPA
5.	Mockito, MockMvc used for test


---

## User Endpoints

### Create a user

*Endpoint:* /user/register

*Http Method:* POST

*Validations:*
1. User Name should be alphanumeric. No spaces or special symbol
2. Password should have at least:
    1. Minimum 4 characters
    2. One upper case character
    3. One lower case character
    4. One digit
3. Valid date of birth

*curl:* curl -k -v http://localhost:9191/user-registration/user/register?username=StephenHawking&password=Physicist1942&dateOfBirth=1942-01-08&ssn=654987123 -X POST


### De-Activate a user

*Endpoint:* /user/{id}/deactivate

*Http Method:* PUT

*curl:* curl -k -v http://localhost:9191/user-registration/user/{1}/deactivate -X PUT


---

## User Exclusion Endpoints

### Get all User Exclusions

*Endpoint:* /exclusion/all

*Http Method:* GET

*curl:* curl -k -v http://localhost:9191/user-registration/exclusion/all -X GET


### Add User Exclusion

*Endpoint:* /exclusion/add

*Http Method:* POST

*Validations:*
1.  Valid date of birth

*curl:* curl -k -v http://localhost:9191/user-registration/exclusion/add?dateOfBirth=1942-21-08&ssn=654987123 -X POST


### Remove User Exclusion

*Endpoint:* /exclusion/remove

*Http Method:* PUT

*Validations:*
1.  Valid date of birth

*curl:* curl -k -v http://localhost:9191/user-registration/exclusion/add?dateOfBirth=1942-21-08&ssn=654987123 -X PUT


---
### H2 Console

*URL:* http://localhost:9191/user-registration/h2-console

---
### PostMan Collection

*Relative path:* /postman-collection/user-registration.postman_collection.json

*Have included a postman collection for the following requests:*

1. Create user validation scenario
2. Create user present in Exclusion list
3. Create a user
4. De-activate a user
5. Get all exclusions
6. Add Exclusion
7. Add Exclusion Validation scenario
8. Remove Exclusion


**Note:** have included in project so that everything is in one place, ideally should be out of code base. 

---

### Script

Have included a script to build and run the application

*Relative path:* /scripts/build_and_start.sh

No need of parameters, simply execute from the above mentioned path.

**Note:** have included in project so that everything is in one place, ideally should be out of code base.

---


## Upcoming/Desirable Features
1. Transaction Management (Currently ot needed as no multiple updates)
2. Spring Security (Probably for adding/removing user exclusions)
3. Audit to keep track of activities 
4. Spring Actuators (health-check)